# python 3
# SEE README FILE FOR DETAILS
# leverages http://adam.kahtava.com/publicly-available-web-services/#whois

import json, requests, re, sys, socket

IP_LIST = "/[path here]/verifiedTaniumServers.txt"
BASE_URL = "http://adam.kahtava.com/services/whois.json?query="

with open(IP_LIST, "r") as lines:
    for line in lines:
        currentIP = line.strip()
        if not len(currentIP) > 0:
            continue
        if not re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",currentIP):
            print("%s;FAIL;THIS IS NOT AN IP;FAIL" % currentIP)
            continue
        try:
            resp = requests.get(url=BASE_URL+currentIP)
            data = json.loads(resp.text.replace("\\n"," ").replace("\\r"," ").replace("\\u000d"," ").replace("\\u000a"," "))

            name = data["RegistryData"]["Registrant"]["Name"]
            address = data["RegistryData"]["Registrant"]["Address"]
            zipCode = data["RegistryData"]["Registrant"]["PostalCode"]
        except:
            name = "FAIL"
            address = "FAIL"
            zipCode = "FAIL"
          
        try:
            rDNS = socket.gethostbyaddr(currentIP)[0]
        except:
            rDNS = "rDNS FAIL"

        print("%s;%s;%s;%s;%s" % (currentIP,name,address,zipCode,rDNS))
        sys.stdout.flush()