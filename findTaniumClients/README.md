SEE ARTICLE HERE: http://penconsultants.com/blog/post/exposing-tanium-a-hackers-paradise

Steps to discover Tanium servers and their customers are below.  I purposely did not include argparse in these scripts to force you to review the script.

1. Potentially change your router/FWs TCP settings.  I had to set the max TCP connections to 32768 and the TCP (FIN_WAIT) timeout to 600 seconds.

2. Run masscan to find open ports. It should give you ~4 million IPs.  
 `masscan -p17472 0.0.0.0/0 --rate 10000 -oG scan_17472 --exclude 127.0.0.0/8 --exclude 169.254.0.0/16 --exclude 255.255.255.255 --exclude 10.0.0.0/8 --exclude 172.16.0.0/12 --exclude 192.168.0.0/16 --exclude 0.0.0.0/8`
3. Parse out IPs
 `cat scan_17472 | awk '{print $2}' > potentialTaniumServers.txt`
4. Run the first script which will attempt to filter out IPs (from above) that do not appear to have Tanium.
 `python3 findTaniumServers.py`
5. Clean up the output from above and extract only IPs from verified Tanium servers.  You should have about four thousand IPs.
 `cat verifiedTaniumServers.log |grep DOES| tr "[" " " | tr "]" " " | awk '{print $4}' > verifiedTaniumServers.txt`
6. Now run the bulkWhois script against those IPs.  Note, about a 50 of the four thousand IPs will fail at the WhoIs lookup.  If you want a complete list, you may have to find another bulk service to use, or do those manually.
 `python3 bulkWhois.py > taniumCustomers.txt`
7. And finally, to view a unique list of their likely customers...
 `cat taniumCustomers.txt | awk -F ";" '{print $2}' |sort | uniq | less`. You should have about 125 unique customers identified. Although the false postive rate is far lower than my first release of this code, there still seems to be a few net ranges giving a high FP rate. Example: "Asia Pacific Network Information Centre" could probably be thrown out. Also note, serveral of the "customers" are webhosts or ISPs being used by their actual customers. Example, GorillaServers is most likely not a Tanium customer themselves.

8. The above gives you a real close sense of their customers. If you want to have even more confidence, checkout column 5 of taniumCustomers.txt. It contains the reverse DNS of each IP, which could give you even more confidense. Here's actual subdomain names of Tanium customer domains`
cfcapps.[DOMAIN HERE]
cftanium.[DOMAIN HERE]
cftaniumaffiliates.[DOMAIN HERE]
cptanzdv.[DOMAIN HERE]
exttaniumzs01.[DOMAIN HERE]
isgtaniumzone1.[DOMAIN HERE]
isgtaniumzone2.[DOMAIN HERE]
IX1TANZONE01.[DOMAIN HERE]
IX1TANZONE02.[DOMAIN HERE]
tanedg.[DOMAIN HERE]
tanium.[DOMAIN HERE]
TANIUM.[DOMAIN HERE]
tanium03.[DOMAIN HERE]
taniumdmz.[DOMAIN HERE]
taniumgt.[DOMAIN HERE]
taniumlb01.[DOMAIN HERE]
taniumlb03.[DOMAIN HERE]
taniumlb04.[DOMAIN HERE]
taniumzs.[DOMAIN HERE]
tanzone.[DOMAIN HERE]
tanzone1.[DOMAIN HERE]
tanzone2.[DOMAIN HERE]
tcz.[DOMAIN HERE]
tm.[DOMAIN HERE]
tserver.[DOMAIN HERE]
tzonedfw.[DOMAIN HERE]
tzonesna.[DOMAIN HERE]
tzs.[DOMAIN HERE]
zone.[DOMAIN HERE]
zonesrv.[DOMAIN HERE]`

When the subdomain is "tanium" and port 17472 is reachable, there should be zero doubt you have found a Tanium server.

---------------------------------------------------------------------

BACKGROUND
In addition to reading the article linked at the top, here's some additional info about how I developed this.  This will help you understand why things are in pieces and separate scripts, as I had to figure things out along the way, and did not take the time to go back and combine them.

As the article points out, the default port for a Tanium server (internal or external to the customer's network), is tcp-17472.  With that info, I ran masscan against the whole internet.

The next step was to figure out what a Tanium server looked like when it responded.  For that, I needed a known Tanium server.  Fortunately for me, Tanium lists several of their customers on their website, and several of their customers brag about using them in various articles and posts. From there, it was just a matter of Googling for those customers in whois records (by name) to find a few likely IP ranges that would have a Tanium server and then cross reference against my masscan results.

After finding a few known Tanium servers, I simply sent a string of null bytes to it and looked for the behavior.  It wasn't long after that when I was able to create a working version of the findTaniumServers.py script.  In May/June of 2018, after looking for a way to reduce the obvious false postives (about 80% FP), I discovered that sending two non-null bytes had a known and unique response from the true Tanium servers. That second check was added to the script, driving the FP rate down to almost nothing (after removing IPs from "Asia Pacific Network").  And, the rest is self explanatory.
