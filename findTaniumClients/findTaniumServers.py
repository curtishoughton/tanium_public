# python 3
# SEE README FILE FOR DETAILS

from time import sleep
import threading
import logging
import socket
import random

IPS_FILE = "potentialTaniumServers.txt"
LOG_FILE = "verifiedTaniumServers.log"
MAX_THREADS = 300  #scale this up until your network/fw state table is nearing saturation (or 40-50% to be safe)

BUFFER_SIZE = 1024
TCP_PORT = 17472
logging.basicConfig(format='%(asctime)s [%(levelname)s][%(threadName)s]  %(message)s', filename=LOG_FILE, level=logging.INFO)

class checkForTanium(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.TCP_IP = name

    def run(self):
        data_snd = bytes.fromhex('0000')
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(5)
        data = None
        try:
            s.connect((self.TCP_IP, TCP_PORT))
            s.send(data_snd)
            data = s.recv(BUFFER_SIZE)
            if(len(data) == 0):
                try:
                    s.shutdown(1)
                    s.close()
                except:
                    pass

                data_snd = bytes.fromhex('9999')
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(5)
                data = None
                try:
                    s.connect((self.TCP_IP, TCP_PORT))
                    s.send(data_snd)
                    data = s.recv(BUFFER_SIZE)
                    retSize = len(data)
                    logging.info("Does NOT appear to have Tanium - Failed check 3")
                except: # if it's a tanium server, this request should timeout and fall here
                    logging.info("DOES appear to have Tanium")
                    pass
            else:
                logging.info("Does NOT appear to have Tanium - Failed check 2")
        except:
            logging.info("Does NOT appear to have Tanium - Failed check 1")
            pass
        try:
            s.shutdown(1)
            s.close()
        except:
            pass

def main():
    logging.getLogger("history").setLevel(logging.DEBUG)

    myThreads = []
    buffer = []
    with open(IPS_FILE, "r") as lines:
        for line in lines:
            buffer.append(line.strip())

    random.shuffle(buffer)

    for line in buffer:
        TCP_IP = line
        objx = checkForTanium(TCP_IP)
        objx._name = TCP_IP
        objx.start()
        myThreads.append(objx)
        print("thread count %s" % threading.activeCount())
        while(threading.activeCount() > MAX_THREADS):
            sleep(0.01)
        sleep(0.001)

if __name__ == "__main__":
    main()
